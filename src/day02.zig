const std = @import("std");
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const print = std.debug.print;

const Line = struct {
    num_1: u8,
    num_2: u8,
    letter: []const u8,
    password: []const u8,
};

pub fn main() !void {
    var which_part: u8 = undefined;
    var args = std.process.args();
    _ = args.skip();
    if (args.nextPosix()) |arg| {
        if (std.mem.startsWith(u8, arg, "part1")) {
            which_part = 1;
        } else if (std.mem.startsWith(u8, arg, "part2")) {
            which_part = 2;
        } else {
            const stderr = std.io.getStdErr().writer();
            stderr.print("error: unexpected argument {s}\n", .{arg}) catch {};
            std.os.exit(1);
        }
    } else {
        const stderr = std.io.getStdErr().writer();
        stderr.print("error: expected argument\n", .{}) catch {};
        std.os.exit(1);
    }

    const allocator = std.heap.page_allocator;

    const input = try std.fs.cwd().readFileAlloc(
        allocator,
        "input_day02",
        std.math.maxInt(usize),
    );
    defer allocator.free(input);

    var lines_iter = std.mem.tokenize(input, "\n");

    var valid_passwords: u16 = 0;
    while (lines_iter.next()) |line| {
        if (try parseLine(line)) |line_parts| {
            print("{}; {}; {s}; {s}: ", .{
                line_parts.num_1,
                line_parts.num_2,
                line_parts.letter,
                line_parts.password,
            });
            if (which_part == 1) {
                valid_passwords += try part1(line_parts);
            } else {
                valid_passwords += try part2(line_parts);
            }
        }
    }
    print("------------------------------\n", .{});
    print("The amount of valid passwords is: {}\n", .{valid_passwords});
}

fn part1(line_parts: Line) !u8 {
    const letter_count = std.mem.count(u8, line_parts.password, line_parts.letter);
    print("{}\n", .{letter_count});

    if (letter_count >= line_parts.num_1 and letter_count <= line_parts.num_2) {
        return 1;
    }
    return 0;
}

fn part2(line_parts: Line) !u8 {
    if ((line_parts.password[line_parts.num_1 - 1] == line_parts.letter[0]) !=
        (line_parts.password[line_parts.num_2 - 1] == line_parts.letter[0]))
    {
        print("{}\n", .{true});
        return 1;
    } else {
        print("{}\n", .{false});
        return 0;
    }
}

fn parseLine(line: []const u8) !?Line {
    var parts_iter = std.mem.tokenize(line, "-: ");
    if (parts_iter.next()) |part| {
        const line_parts = Line{
            .num_1 = try std.fmt.parseInt(u8, part, 10),
            .num_2 = try std.fmt.parseInt(u8, parts_iter.next().?, 10),
            .letter = parts_iter.next().?,
            .password = parts_iter.next().?,
        };
        return line_parts;
    } else {
        return null;
    }
}
